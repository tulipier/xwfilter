#ifndef __FILTERS_H_
#define __FILTERS_H_

#include <xcb/xproto.h>

typedef struct node {
    xcb_window_t val;
    struct node * next;
} node_t;

typedef struct winlist {
    node_t* start;
} winlist_t;

winlist_t* winlist_from_stdin(int uniq, char sep);
void debug_winlist(winlist_t* list);
char* filter_org_from_class(char* class_name);

#endif // __FILTERS_H_
