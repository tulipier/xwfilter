#include "getxprop.h"

#include <xcb/xcb_atom.h>
#include <xcb/xproto.h>
#include <xcb/xcb_icccm.h>
#include <xcb/xcb_ewmh.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>


xcb_connection_t *dpy;
int default_screen;
xcb_screen_t *screen;

bool check_connection (xcb_connection_t *dpy)
{
    int xerr;
    if ((xerr = xcb_connection_has_error(dpy)) != 0) {
        fprintf(stderr, "The server closed the connection: ");
        switch (xerr) {
            case XCB_CONN_ERROR:
                fprintf(stderr, "socket, pipe or stream error.\n");
                break;
            case XCB_CONN_CLOSED_EXT_NOTSUPPORTED:
                fprintf(stderr, "unsupported extension.\n");
                break;
            case XCB_CONN_CLOSED_MEM_INSUFFICIENT:
                fprintf(stderr, "not enough memory.\n");
                break;
            case XCB_CONN_CLOSED_REQ_LEN_EXCEED:
                fprintf(stderr, "request length exceeded.\n");
                break;
            case XCB_CONN_CLOSED_PARSE_ERR:
                fprintf(stderr, "can't parse display string.\n");
                break;
            case XCB_CONN_CLOSED_INVALID_SCREEN:
                fprintf(stderr, "invalid screen.\n");
                break;
            case XCB_CONN_CLOSED_FDPASSING_FAILED:
                fprintf(stderr, "failed to pass FD.\n");
                break;
            default:
                fprintf(stderr, "unknown error.\n");
                break;
        }
        return false;
    } else {
        return true;
    }
}

char* get_window_title(xcb_window_t window){
    char* title = NULL;
    xcb_icccm_get_text_property_reply_t reply;
    if (0 && xcb_icccm_get_wm_name_reply(dpy, xcb_icccm_get_wm_name(dpy, window), &reply, NULL) == 1) {
        printf("%s\t", reply.name);
        title = strdup(reply.name);
        xcb_icccm_get_text_property_reply_wipe(&reply);
    }else{
        fprintf(stderr, "fail to find titile for : %d\n", window);
        xcb_atom_t property = XCB_ATOM_WM_NAME;
        xcb_atom_t type = XCB_ATOM_STRING;
        xcb_get_property_reply_t *reply;
        xcb_get_property_cookie_t cookie;
        cookie = xcb_get_property(dpy, 0, window, property, type, 0, 1024);
        if ((reply = xcb_get_property_reply(dpy, cookie, NULL))) {
            int len = xcb_get_property_value_length(reply);
            fprintf(stdout, "+(_)+%.*s\t", len,
                    (char*)xcb_get_property_value(reply));
        }else{
            fprintf(stderr, "--UNKWOWN--\t");
        }
        free(reply);

    }
    return title;
}

/*
 * Cache for atom lookups in either direction
 */
struct atom_cache_entry {
    xcb_atom_t atom;
    const char *name;
    xcb_intern_atom_cookie_t intern_atom;
    struct atom_cache_entry *next;
};

static struct atom_cache_entry *atom_cache;

/*
 * Send a request to the server for an atom by name
 * Does not create the atom if it is not already present
 */
struct atom_cache_entry *Intern_Atom (xcb_connection_t * dpy, const char *name)
{
    struct atom_cache_entry *a;

    for (a = atom_cache ; a != NULL ; a = a->next) {
        if (strcmp (a->name, name) == 0)
            return a; /* already requested or found */
    }

    a = calloc(1, sizeof(struct atom_cache_entry));
    if (a != NULL) {
        a->name = name;
        a->intern_atom = xcb_intern_atom (dpy, 0, strlen (name), (name));
        a->next = atom_cache;
        atom_cache = a;
    }
    return a;
}

/* Get an atom by name when it is needed. */
xcb_atom_t Get_Atom (xcb_connection_t * dpy, const char *name)
{
    struct atom_cache_entry *a = Intern_Atom (dpy, name);

    if (a == NULL)
        return XCB_ATOM_NONE;

    if (a->atom == XCB_ATOM_NONE) {
        xcb_intern_atom_reply_t *reply;

        reply = xcb_intern_atom_reply(dpy, a->intern_atom, NULL);
        if (reply) {
            a->atom = reply->atom;
            free (reply);
        } else {
            a->atom = (xcb_atom_t) -1;
        }
    }
    if (a->atom == (xcb_atom_t) -1) /* internal error */
        return XCB_ATOM_NONE;

    return a->atom;
}

/* Get the name for an atom when it is needed. */
const char *Get_Atom_Name (xcb_connection_t * dpy, xcb_atom_t atom)
{
    struct atom_cache_entry *a;

    for (a = atom_cache ; a != NULL ; a = a->next) {
        if (a->atom == atom)
            return a->name; /* already requested or found */
    }

    a = calloc(1, sizeof(struct atom_cache_entry));
    if (a != NULL) {
        xcb_get_atom_name_cookie_t cookie = xcb_get_atom_name (dpy, atom);
        xcb_get_atom_name_reply_t *reply
            = xcb_get_atom_name_reply (dpy, cookie, NULL);

        a->atom = atom;
        if (reply) {
            int len = xcb_get_atom_name_name_length (reply);
            char *name = malloc(len + 1);
            if (name) {
                memcpy (name, xcb_get_atom_name_name (reply), len);
                name[len] = '\0';
                a->name = name;
            }
            free (reply);
        }

        a->next = atom_cache;
        atom_cache = a;

        return a->name;
    }
    return NULL;
}


xcb_atom_t atom_net_wm_name, atom_utf8_string, atom_net_wm_desktop, atom_net_desktop_names;

/* Gets UTF-8 encoded EMWH property _NET_WM_NAME for a window */
static xcb_get_property_cookie_t
get_net_wm_name (xcb_connection_t *gnwn_dpy, xcb_window_t win)
{
    if (atom_net_wm_name && atom_utf8_string)
        return xcb_get_property (gnwn_dpy, 0, win, atom_net_wm_name,
                                 atom_utf8_string, 0, BUFSIZ);
    else {
        xcb_get_property_cookie_t dummy = { 0 };
        return dummy;
    }
}

static xcb_get_property_cookie_t
get_net_wm_desktop(xcb_connection_t *dpy, xcb_window_t win){
    if (atom_net_wm_desktop)
        return xcb_get_property(dpy, 0, win, atom_net_wm_desktop, XCB_ATOM_CARDINAL, 0, 4);
    else {
        xcb_get_property_cookie_t dummy = { 0 };
        return dummy;
    }
}

// from : https://stackoverflow.com/questions/3774417/sprintf-with-automatic-memory-allocation
int
vasprintf(char **strp, const char *fmt, va_list ap)
{
    va_list ap1;
    size_t size;
    char *buffer;

    va_copy(ap1, ap);
    size = vsnprintf(NULL, 0, fmt, ap1) + 1;
    va_end(ap1);
    buffer = calloc(1, size);

    if (!buffer)
        return -1;

    *strp = buffer;

    return vsnprintf(buffer, size, fmt, ap);
}

int
asprintf(char **strp, const char *fmt, ...)
{
    int error;
    va_list ap;

    va_start(ap, fmt);
    error = vasprintf(strp, fmt, ap);
    va_end(ap);

    return error;
}




char* get_win_name(xcb_window_t win){

    xcb_get_property_cookie_t net_wm_name_cookie = get_net_wm_name(dpy, win);
    xcb_icccm_get_text_property_reply_t wmn_reply;
    uint8_t got_reply = 0;
    xcb_get_property_reply_t *prop;
    const char *wm_name = NULL;
    unsigned int wm_name_len = 0;
    xcb_atom_t wm_name_encoding = XCB_NONE;
    char *result=NULL;

    /* Get window name if any */
    prop = xcb_get_property_reply (dpy, net_wm_name_cookie, NULL);
    if (prop && (prop->type != XCB_NONE)) {
        wm_name = xcb_get_property_value (prop);
        wm_name_len = xcb_get_property_value_length (prop);
        wm_name_encoding = prop->type;
    } else { /* No _NET_WM_NAME, check WM_NAME */
        got_reply = xcb_icccm_get_wm_name_reply (dpy, xcb_icccm_get_wm_name(dpy, win),
                                                 &wmn_reply, NULL);
        if (got_reply) {
            wm_name = wmn_reply.name;
            wm_name_len = wmn_reply.name_len;
            wm_name_encoding = wmn_reply.encoding;
        }
    }
    if (wm_name_len == 0) {
        return strdup (" (has no name)");
    } else {
        if (wm_name_encoding == XCB_ATOM_STRING) {
            asprintf (&result, "%.*s", wm_name_len, wm_name);
        } else if (wm_name_encoding == atom_utf8_string) {
            //print_utf8 (" \"", wm_name, wm_name_len,  "\"");
            asprintf (&result, "%.*s", wm_name_len, wm_name);
        } else {
            /* Encodings we don't support, including COMPOUND_TEXT */
            const char *enc_name = Get_Atom_Name (dpy, wm_name_encoding);
            if (enc_name) {
                asprintf (&result, " (name in unsupported encoding %s)", enc_name);
            } else {
                asprintf (&result, " (name in unsupported encoding ATOM 0x%x)",
                        wm_name_encoding);
            }
        }
    }
    if (got_reply)
        xcb_icccm_get_text_property_reply_wipe (&wmn_reply);

    return result;

}
void print_win_name(xcb_window_t win){
    char *name = get_win_name(win);
    printf(name);
    free(name);
}

char* get_win_class(xcb_window_t win){
    xcb_icccm_get_wm_class_reply_t classhint;
    xcb_get_property_reply_t *classprop;
    if (xcb_icccm_get_wm_class_reply (dpy, xcb_icccm_get_wm_class (dpy, win),
                                      &classhint, NULL)) {
        if(classhint.instance_name){
            //printf("inst: %.*s", (int)strlen(classhint.instance_name), classhint.instance_name);
        }
        if(classhint.class_name){
            //printf("%.*s", (int)strlen(classhint.class_name), classhint.class_name);
            return classhint.class_name;
        }
    }
    return NULL;
}

uint32_t get_win_desktop(xcb_window_t win){
    xcb_get_property_reply_t *prop;
    if (atom_net_wm_desktop) {
        prop = xcb_get_property_reply (dpy, get_net_wm_desktop(dpy, win), NULL);
        if (prop && (prop->type != XCB_NONE)) {
            uint32_t *desktop = xcb_get_property_value (prop);
            free (prop);
            return *desktop;
        }else{
            free (prop);
        }
    }
    return 0xFFFFFFE;
}
char* get_desktop_name(uint32_t desktop){
    char *result = malloc(20*sizeof(char));
    if (desktop == 0xFFFFFFFF) {
        result = "*";
    }else if (desktop == 0xFFFFFFE){
        result = "‑";
    }else {
        sprintf(result, "%d", desktop+1);
    }
    return result;
}

xcb_ewmh_connection_t *ewmh;

void ewmh_init()
{
	ewmh = calloc(1, sizeof(xcb_ewmh_connection_t));
	if (xcb_ewmh_init_atoms_replies(ewmh, xcb_ewmh_init_atoms(dpy, ewmh), NULL) == 0) {
		fprintf(stderr, "Can't initialize EWMH atoms.\n");
	}
}
void print_desktop_names(){
    xcb_ewmh_get_utf8_strings_reply_t reply;
    if (xcb_ewmh_get_desktop_names_reply(ewmh, xcb_ewmh_get_desktop_names(ewmh, 0), &reply, NULL)) {
        uint32_t i = 0;
        fprintf(stderr, "len : %d , str : %s \n", reply.strings_len, reply.strings);
        char* desktop = reply.strings;
        size_t len = 0;
        while(i < reply.strings_len){
            len = strlen(desktop+i);
            if(len+i>reply.strings_len){
                len = reply.strings_len - i;
            }
            fprintf(stderr, "-- %.*s\n", len, desktop+i);
            i += len + 1;
        }
    }
}

desktop_name_t** get_desktop_names(int screen_n){
    xcb_ewmh_get_utf8_strings_reply_t reply;
    if (xcb_ewmh_get_desktop_names_reply(ewmh, xcb_ewmh_get_desktop_names(ewmh, screen_n), &reply, NULL)) {
        desktop_name_t** names = malloc((int)(reply.strings_len/2) * sizeof(desktop_name_t*));
        uint32_t i = 0;
        uint32_t j = 0;
        //fprintf(stderr, "len : %d , str : %s \n", reply.strings_len, reply.strings);
        char* desktop = reply.strings;
        size_t len = 0;
        while(i < reply.strings_len){
            names[j] = malloc(sizeof(desktop_name_t));
            len = strlen(desktop+i);
            if(len+i>reply.strings_len){
                len = reply.strings_len - i;
            }
            names[j]->str = desktop+i;
            names[j]->len = len;
            //fprintf(stderr, "-- %.*s\n", len, desktop+i);
            j++;
            i += len + 1;
        }
        return names;
    }else{
        return NULL;
    }
}
