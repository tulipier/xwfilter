#ifndef __GETXPROP_H_
#define __GETXPROP_H_

#include <xcb/xcb.h>
#include <xcb/xcb_ewmh.h>
#include <xcb/xcb_atom.h>
#include <xcb/xcb_icccm.h>
#include <stdbool.h>


extern xcb_connection_t *dpy;
extern int default_screen;
extern xcb_screen_t *screen;
extern xcb_atom_t atom_net_wm_name, atom_utf8_string, atom_net_wm_desktop, atom_net_desktop_names;

typedef struct desktop_name {
    size_t len;
    char* str;
} desktop_name_t;

bool check_connection (xcb_connection_t *dpy);
char* get_window_title(xcb_window_t window);
void print_win_name(xcb_window_t win);
char* get_win_name(xcb_window_t win);
char* get_win_class(xcb_window_t win);
uint32_t get_win_desktop(xcb_window_t win);
char* get_desktop_name(uint32_t desktop);

desktop_name_t** get_desktop_names(int screen_n);
void print_desktop_names();
void ewmh_init();

xcb_atom_t Get_Atom (xcb_connection_t * dpy, const char* name);

#endif // __GETXPROP_H_
