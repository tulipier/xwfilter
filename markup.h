#ifndef __MARKUP_H_
#define __MARKUP_H_

#include <stddef.h>

char* markup_escape_text(const char* text, size_t length);

#endif // __MARKUP_H_
