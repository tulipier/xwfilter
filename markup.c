#include "markup.h"

#include <string.h>
#include <stdlib.h>

char* markup_escape_text (const char *text, size_t length){
  char *str;

  if(text==NULL)
      return NULL;

  if (length <= 0)
    length = strlen (text);


  const char *pending;
  const char *end;
  ssize_t extra_len = 0;

  pending = text;
  end = text + length;


  while (pending < end)
  {
      unsigned char c = (unsigned char) *pending;
      switch (c)
      {
          case '&':
              extra_len+=4;
              break;
          case '<':
              extra_len+=3;
              break;
          case '>':
              extra_len+=3;
              break;
      }
      pending++;
  }


  size_t total_len = length+extra_len;
  /* alloc string accordingly to extra_len need */
  str = malloc(total_len * sizeof(char));
  char *cursor = str;
  pending = text;
  end = text + length;

  while (pending < end){
      unsigned char c = (unsigned char) *pending;
      switch (c){
          case '&':
              strcpy(cursor, "&amp;");
              cursor+=4;
              break;
          case '<':
              strcpy(cursor, "&lt;");
              cursor+=3;
              break;
          case '>':
              strcpy(cursor, "&gt;");
              cursor+=3;
              break;
          default:
              cursor[0] = c;
      }
      cursor++;
      pending++;
  }
  str[total_len] = '\0';

  return str;
}
