#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "filters.h"
#include "getxprop.h"
#include "markup.h"

#define DEBUG 0
#define debug_print(fmt, ...)                                       \
    do { if (DEBUG) fprintf(stderr, fmt, __VA_ARGS__); } while (0)

int main (int argc, char **argv){
    int uniq = 0;
    int filter_window_class = 0;
    xcb_window_t window_class_id = 0;
    char *window_class = NULL;
    char sep = '\n';
    int last = 0;
    int index;
    char *file_path = NULL;
    int c;
    int filter_org = 0;
    int filter_desktop = 0;
    int display_desktop = 0;
    int init_ewmh = 0;
    int get_desktop = 0;
    int use_markup = 0;
    desktop_name_t** desktop_names;
    FILE *fp = NULL;

    opterr = 0;


    while ((c = getopt (argc, argv, "mus:f:w:lodD")) != -1){
        switch (c)
        {
            case 'u':
                uniq = 1;
                break;
            case 'm':
                use_markup = 1;
                break;
            case 'w':
                window_class_id = (xcb_window_t) strtol(optarg, NULL, 10);
                break;
            case 's':
                sep = optarg[0];
                break;
            case 'f':
                file_path = optarg;
                break;
            case 'l':
                last = 1;
                break;
            case 'd':
                init_ewmh = 1;
                get_desktop = 1;
                display_desktop = 1;
                break;
            case 'D':
                init_ewmh = 1;
                get_desktop = 1;
                filter_desktop = 1;
                break;
            case 'o':
                filter_org = 1;
                break;
            case '?':
                if (optopt == 'c')
                    fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                else if (optopt == 's')
                    fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf (stderr,
                             "Unknown option character `\\x%x'.\n",
                             optopt);
                return 1;
            default:
                abort ();
        }
    }

    debug_print("uniq = %d, filter_window_class = %d, window_class = '%s', sep = '%c'\n",
                uniq, filter_window_class, window_class, sep);

    if(last != 0 && window_class_id != 0){
        fprintf(stderr, "You can't use -l and -w (window) option together");
        return 1;
    }

    for (index = optind; index < argc; index++){
        fprintf (stderr, "Non-option argument %s\n", argv[index]);
    }

    winlist_t *winlist = winlist_from_stdin(uniq, sep);
    //debug_winlist(winlist);


    dpy = xcb_connect(NULL, &default_screen);
    if (!check_connection(dpy)) {
        exit(EXIT_FAILURE);
    }

    // init Atoms, EWMH and desktop names
    atom_net_wm_name = Get_Atom (dpy, "_NET_WM_NAME");
    atom_utf8_string = Get_Atom (dpy, "UTF8_STRING");

    if(get_desktop){
        atom_net_wm_desktop = Get_Atom (dpy, "_NET_WM_DESKTOP");
    }
    if(init_ewmh){
        ewmh_init();
        if(display_desktop == 1){
            // TODO : replace '0' by the real current screen number
            desktop_names = get_desktop_names(0);
            atom_net_desktop_names = Get_Atom (dpy, "_NET_DESKTOP_NAMES");
        }
    }
    // --


    if(file_path != NULL){
        fp = fopen(file_path, "w");
    }

    char* markup_bold_start="";
    char* markup_bold_end="";
    if(use_markup == 1){
        markup_bold_start="<b>";
        markup_bold_end="</b>";
    }

    if (winlist != NULL){
        node_t *node = winlist->start;
        char* class_name;
        char* icon_name;
        uint32_t win_desktop = 0 ;
        uint32_t current_desktop = 0;
        char *win_name=NULL;
        if (last != 0){
            window_class = get_win_class(winlist->start->val);
        }else if(window_class_id != 0){
            window_class = get_win_class(window_class_id);
        }
        if(filter_desktop){
            current_desktop = get_win_desktop(winlist->start->val);
        }
        while(node != NULL){
            class_name = get_win_class(node->val);
            icon_name = class_name;
            if(get_desktop == 1){
                win_desktop = get_win_desktop(node->val);
            }
            if((filter_desktop == 0 || win_desktop == 0xFFFFFFFF || win_desktop == current_desktop) && \
               (window_class == NULL || class_name == NULL || strcmp(class_name, window_class) == 0)){
                // Do not check class_name or class name identical, so display window infos
                if(filter_org == 1){
                    class_name = filter_org_from_class(class_name);
                }
                // Print class name (with ou without bold markup)
                printf(" %s%c%-15s%s ⋅ ", markup_bold_start, toupper(class_name[0]), class_name+1, markup_bold_end);
                if(display_desktop == 1){
                    printf("%.*s ⋅ ", desktop_names[win_desktop]->len, desktop_names[win_desktop]->str);
                }
                // Print window name (escape if markup)
                win_name = get_win_name(node->val);
                if(use_markup == 1){
                    char* escaped_win_name = markup_escape_text(win_name, 0);
                    printf("%s", escaped_win_name);
                    free(escaped_win_name);
                }else{
                    printf("%s",win_name);
                }
                free(win_name);
                // Print icon
                printf("%cicon\x1f", '\0');
                printf("%c%.*s", tolower(icon_name[0]), (int)strlen(icon_name)-1, icon_name+1);
                printf("\n");
                if(file_path != NULL){
                    fprintf(fp, "%d\n", node->val);
                }
            }
            node = node->next;
        }
    }

    if(fp != NULL){
        fclose(fp);
    }

    return 0;
}
