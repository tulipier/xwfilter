#include "filters.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

node_t* create_node(xcb_window_t win){
    node_t* node = malloc(sizeof(node_t));
    if(node == NULL){
        fprintf(stderr, "FAIL TO ALLOCATE !\n\n\n");
    }
    node->val = win;
    node->next = NULL;
    return node;
}

int is_uniq_and_add(xcb_window_t win, winlist_t *list){
    if(list->start == NULL){
        //printf("** Create list\n");
        list->start = create_node(win);
        return 1;
    }
    //printf("- List curr state :\n");
    //debug_winlist(list);
    node_t **node = &(list->start);
    do {
        //printf("\tTest node(val) %d =? %d (win)\n", (*node)->val, win);
        if((*node)->val == win){
            return 0;
        }
        node = &((*node)->next);
    } while(*node != NULL);
    *node = create_node(win);
    return 1;
}

void winlist_free(winlist_t* list){
    fprintf(stderr, "\n\n --- FREE LIST --- \n\n\n");
    if(list->start != NULL){
        node_t *node = list->start;
        do {
            free(node);
        } while(node->next != NULL);
    }
    free(list);
}


int try_to_add_string_to_list(winlist_t* list, char *cursor, int uniq){
    xcb_window_t win = (xcb_window_t) strtol(cursor, NULL, 10);
    if(win == 0 && errno == EINVAL){
        fprintf(stderr, "Incorrect data, no window id before sep\n");
        return -1;
    }
    //printf("Add : %d to list %p\n", win, list);
    if(uniq){
        if(is_uniq_and_add(win, list) != 1){
            //printf("not uniq");
        }else{
            //debug_winlist(list);
        }

    }else{
        /* **next = create_node(win); */
        /* *next = &((**next)->next); */
    }
    //printf("--\n");
    return 0;
}

void debug_winlist(winlist_t* list){
    if (list != NULL){
        printf("--- Winlist ---\n");
        node_t *node = list->start;
        int i=0;
        while(node != NULL){
            printf("\t> %d\n", node->val);
            node = node->next;
            i++;
        }
        printf("--- END, len = %d ---\n", i);
    }else{
        printf("Empty winlist \n");
    }
}


winlist_t* winlist_from_stdin(int uniq, char sep){
    winlist_t* list = malloc(sizeof(winlist_t));
    list->start = NULL;

    ssize_t read;
    char *line = NULL;
    size_t len = 0;
    int c;
    while ((read = getline(&line, &len, stdin)) != -1) {
        //printf("Get line : %s", line);
        if(read >= 2 && line[0] == '0' && line[1] == '\n')
            continue;
        if(try_to_add_string_to_list(list, line, uniq) != 0){
            winlist_free(list);
            return NULL;
        }
    }
    return list;
}

char* filter_org_from_class(char* class_name){
    char* result = class_name;
    if(strlen(class_name) > 7){
        if( (class_name[0] != 'O' && class_name[0] != 'o' ) || \
           class_name[1] != 'r' || class_name[2] != 'g' || \
           class_name[3] != '.' ){
        }else{
            result = strrchr(class_name, '.');
            if(result != NULL){
                result += 1;
            }else{
                result = class_name;
            }
        }
    }
    return result;
}
